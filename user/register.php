<?php require('../view/header.php'); ?>

<h2>Register</h2>

<p class="error"><?php echo $register_error; ?></p>

<div class="clear"></div>

<form method="post" action="./" class="std-form">
	
	<input type="hidden" name="action" value="user_create">
	
	<div class="user-info">
		
		<div>
			<label>Username</label>
			<input type="text" name="username" placeholder="Username"
				value="<?php echo $username; ?>">
		</div>
		
		<div>
			<label>E-mail</label>
			<input type="text" name="email" placeholder="Email address"
				value="<?php echo $email; ?>">
		</div>
		
	</div>
	
	<div class="password">
		
		<div>
			<label>Password</label>
			<input type="password" name="password" placeholder="Password">
		</div>
		
		<div>
			<label>Confirm password</label>
			<input type="password" name="confirm" placeholder="Confirm">
		</div>
		
	</div>
	
	<input class="button start" type="submit" value="Save">

</form>

<?php include '../view/footer.php'; ?>