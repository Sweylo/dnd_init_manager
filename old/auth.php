<?php

include('initialize.php');

$user = $_POST['username'];
$pass = $_POST['password'];
$player = Player::getByUsername($user);

if ($player) {

    if (sha1($user . $pass) == $player -> pass) {
        setcookie(
            'user',
            $user,
            time() + (36000),
            $GLOBALS['sub']
        );
        header('Location: ' . $GLOBALS['home']);
    } else {
        echo 'Wrong password for user: ' . $user . '<br />';
    }

} else {
    echo 'Unable to find user: ' . $user;
}

?>
