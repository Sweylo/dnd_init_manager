<?php

include('global.php');

$length = $_POST['length'];

for ($i = 0; $i < $length; $i++) {

    $old = Player::getById($_POST['id_' . $i]);

    if ($_POST['id_' . $i] == $pUser -> id || $pUser -> perm > 1) {

        if ($old -> sec_rank > 0) {
            $secondary = $old -> sec_rank;
        } else {
            $secondary = $_POST['secondary_' . $_POST['id_' . $i]];
        }

        $hidden = 0;
        if ($pUser -> perm > 1 && $_POST['hidden_' . $i] == 'on') {
            $hidden = 1;
        }

        echo '"' . $_POST['hidden_' . $_POST['id_' . $i]] . '"<br />';

        $player = new Player(
            null,
            $_POST['name_' . $i],
            $_POST['char_name_' . $i],
            $_POST['mod_' . $i],
            $_POST['roll_' . $i],
            null,
            null,
            null,
            $hidden,
            $secondary
        );

        if ($player -> update($_POST['id_' . $i])) {
            continue;
        } else {
            die ('Error updating player: id=' . $i);
        }

    }

}

header ('Location: ' . $GLOBALS['home']);

?>
