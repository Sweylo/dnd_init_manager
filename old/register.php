<?php

include('initialize.php');

$username = $_POST['username'];
$key = $_POST['key'];

if ($key == 'valar_morghulis') {

    if ($_POST['password'] == $_POST['confirm']) {

        $player = new Player(
            null,
            $username,
            $username . '_char',
            0,
            0,
            $username,
            sha1($username . $_POST['password']),
            1,
            0,
            0
        );

        if ($player -> write()) {
            header('Location: ' . $GLOBALS['home']);
        } else {
            header(
                'Location: ' .
                "create_user.php?error=0&username=$username&key=$key"
            );
        }

    } else {
        header(
            'Location: ' .
            "create_user.php?error=1&username=$username&key=$key"
        );
    }

} else {
    header(
        'Location: ' .
        "create_user.php?error=2&username=$username&key=$key"
    );
}

?>
