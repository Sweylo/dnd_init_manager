<?php

include('global.php');

if ($_GET['id'] > 2 && $pUser -> perm > 1) {
    if (Player::remove($_GET['id'])) {
        header('Location: ' . $GLOBALS['home']);
    }
} else {
    echo 'Nope.';
}

?>
