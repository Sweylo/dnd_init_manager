<?php

include('initialize.php');

if (isset($_COOKIE['user'])) {
    header('Location: ' . $GLOBALS['home']);
}

?>

<!DOCTYPE html>

<html>

    <head>
        <title>D&amp;D - Login</title>
        <link href="css/reset.css" type="text/css" rel="stylesheet" />
        <link href="css/login.css" type="text/css" rel="stylesheet" />
    </head>

    <body><form action="auth.php" method="post">

        <h1>D&amp;D - Login</h1>

        <input type="text" name="username" placeholder="Username">
        <input type="password" name="password" placeholder="Password">
        <input type="submit" value="Login">

        <a href="create_user.php">Create account</a>

    </form></body>

</html>
