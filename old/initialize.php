<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set("display_errors", 1);
date_default_timezone_set('UTC');

include('classes/Player.php');
include('home.php');

/*
    0: http hostname
    1: http sub folder
    2: mysql server host
    3: mysql username
    4: mysql password
    5: mysql table
    6: mysql port
*/

$conn = new mysqli(
    $GLOBALS['settings'][2],
    $GLOBALS['settings'][3],
    $GLOBALS['settings'][4],
    $GLOBALS['settings'][5],
    $GLOBALS['settings'][6]
);

if ($conn -> connect_error) {
    die("Connection failed: " . $conn -> connect_error);
}
$GLOBALS['conn'] = $conn;

?>
