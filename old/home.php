<?php

    $settingsFile = fopen('config', 'r')
        or die('Unable to open config file');
    $GLOBALS['settings'] = array();
    while (!feof($settingsFile)) {
        array_push($GLOBALS['settings'], trim(fgets($settingsFile)));
    }

    /*
        0: http hostname
        1: http sub folder
        2: mysql server host
        3: mysql username
        4: mysql password
        5: mysql table
        6: mysql port
    */

    $GLOBALS['root'] = $GLOBALS['settings'][0];
    $GLOBALS['sub'] = $GLOBALS['settings'][1];
    $GLOBALS['home'] = 'http://' . $GLOBALS['root'] . $GLOBALS['sub'];

?>
