<?php include('initialize.php'); ?>

<!DOCTYPE html>

<html>

    <head>
        <title>D&amp;D - Create User</title>
        <link href="css/reset.css" type="text/css" rel="stylesheet" />
        <link href="css/create.css" type="text/css" rel="stylesheet" />
    </head>

    <body><form action="register.php" method="post">

        <h1>D&amp;D - Create User</h1>

        <input type="text" name="username" placeholder="Username"
            value="<?php echo $_GET['username']; ?>">
        <input type="password" name="password" placeholder="Password">
        <input type="password" name="confirm" placeholder="Confirm password">
        <input type="text" name="key" placeholder="Security key"
            value="<?php echo $_GET['key']; ?>">
        <input type="submit" value="Create">

        <span id="error">
        <?php
            switch ($_GET['error']) {
                case '0':
                    echo 'Username already taken.';
                    break;
                case '1':
                    echo 'Password not entered twice correctly.';
                    break;
                case '2':
                    echo 'Security key is incorrect.';
                    break;
                default:
                    break;
            }
        ?>
        </span>

    </form></body>

</html>
