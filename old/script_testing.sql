-- Main table

SELECT *, modifier + roll AS total
FROM players
WHERE id > 1
ORDER BY total DESC, secondary_rank ASC;

-- Conflict table

SELECT *, modifier + roll AS total 
FROM (
	SELECT modifier + roll AS dup_total
	FROM players
	WHERE id > 1 AND roll != 0 AND secondary_rank = 0
	GROUP BY dup_total
	HAVING COUNT(dup_total) >= 2
) AS dup, players
WHERE players.modifier + players.roll = dup_total
ORDER BY total DESC, id ASC;