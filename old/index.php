<?php

include('global.php');

if ($pUser -> perm > 1) {
    $data = Player::getAdminAll();
} else {
    $data = Player::getAll();
}

$dup = Player::getConflicts();

?>

<!DOCTYPE html>

<html>

    <head>
        <?php if ($pUser -> user == 'spectator') { ?>
        <meta http-equiv="refresh" content="1">
        <?php } ?>
        <title>D&amp;D</title>
        <link href="css/reset.css" type="text/css" rel="stylesheet" />
        <link href="css/style.css" type="text/css" rel="stylesheet" />
        <?php
        if ($pUser -> perm <= 1) {
            echo '<style>.id { display: none; }</style>';
        }
        ?>
    </head>

    <body><form id="main" action="update.php" method="post">

        <input type="text" class="hidden" name="length"
            value="<?php echo count($data); ?>">

        <div id="header">

            <h1>
                D&amp;D
                <?php
                    if ($pUser -> user == 'dm') {
                        echo ' - DM';
                    } else {
                        echo ' - ' . $pUser -> user;
                    }
                ?>
            </h1>

            <ul>
                <li><a href="logout.php">Logout</a></li>
                <?php if ($pUser -> perm > 1) { ?>
                <li><a href="clear_roll.php">Clear Rolls</a></li>
                <?php } ?>
                <!--<li><a href="change_pw.php">Change PW</a></li>-->
            </ul>

            <div class="clear"></div>

        </div>

        <table>
            <thead>
                <tr>
                    <th class="id" width="1px">ID</th>
                    <th width="80px">Name</th>
                    <th>Character Name</th>
                    <th width="72px">Modifier</th>
                    <th width="50px">Roll</th>
                    <th width="1px">Init</th>
                    <?php if ($pUser -> perm > 1) { ?>
                    <th width="1px">Hidden</th>
                    <?php } ?>
                    <?php if ($pUser -> perm > 0) { ?>
                    <th width="60px">
                        <input type="submit" value="Save">
                    </th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php
                for ($i = 0; $i < count($data); $i++) {
                    echo '<tr';
                    if (
                        $data[$i] -> roll == 0
                        &&
                        $data[$i] -> id == $pUser -> id
                    ) {
                        echo ' class="orange"';
                    } else if (
                        $data[$i] -> roll == 0
                        &&
                        $data[$i] -> id != $pUser -> id
                    ) {
                        echo ' class="red"';
                    }
                    echo '>';
                    if (
                        ($pUser -> id == $data[$i] -> id || $pUser -> perm > 3)
                        ||
                        ($data[$i] -> perm == 0 && $pUser -> perm > 1)
                    ) {
                        echo
                            '<td class="id">' .
                                '<input type="text" readonly value="' .
                                    $data[$i] -> id .
                                    '" name="id_' . $i . '">' .
                            '</td>' .
                            '<td>' .
                                '<input type="text" value="' .
                                    $data[$i] -> name .
                                    '" name="name_' . $i . '">' .
                            '</td>' .
                            '<td>' .
                                '<input type="text" value="' .
                                    $data[$i] -> char_name .
                                    '" name="char_name_' . $i . '">' .
                            '</td>' .
                            '<td>' .
                                '<input type="number" value="' .
                                    //(($data[$i] -> mod < 0) ? '' : '+') .
                                    $data[$i] -> mod .
                                    '" name="mod_' . $i . '">' .
                            '</td>' .
                            '<td>' .
                                '<input type="number" value="' .
                                    $data[$i] -> roll .
                                    '" name="roll_' . $i . '">' .
                            '</td>' .
                            '<td class="total">'
                                . $data[$i] -> getTotal() . '</td>';
                        if (
                            $pUser -> perm > 1 &&
                            $data[$i] -> hidden == 1
                        ) {
                            echo '<td><input name="hidden_' . $i . '"
                                    type="checkbox" checked></td>';
                        } else if (
                            $pUser -> perm > 1 &&
                            $data[$i] -> hidden == 0
                        ) {
                            echo '<td><input name="hidden_' . $i . '"
                                    type="checkbox"></td>';
                        }
                        if (
                            $pUser -> perm > 3
                            ||
                            ($data[$i] -> perm == 0 && $pUser -> perm > 1)
                        ) {
                            echo
                            '<td><a class="delete" href="delete_player.php?id='
                            . $data[$i] -> id . '">Delete</a></td>';
                        } else {
                            echo '<td></td>';
                        }
                        '</tr>';
                    } else {
                        echo
                            '<td class="id">' .
                                '<input type="text" readonly value="' .
                                    $data[$i] -> id .
                                    '" name="id_' . $i . '">' .
                            '</td>' .
                            '<td>' . $data[$i] -> name . '</td>' .
                            '<td>' . $data[$i] -> char_name . '</td>' .
                            '<td>' . $data[$i] -> mod . '</td>' .
                            '<td>' . $data[$i] -> roll . '</td>' .
                            '<td class="total">' . $data[$i] -> getTotal() .
                                '</td>';
                            if ($pUser -> perm > 0) {
                                echo '<td></td>';
                            }
                        echo '</tr>';
                    }
                }
                ?></form>
                <?php if ($pUser -> perm > 1) { ?>
                <tr id="add_new">
                    <form action="add_npc.php" method="post">
                        <td>--</td>
                        <td colspan="2">
                            <input type="text" name="char_name"
                                placeholder="NPC Name">
                        </td>
                        <td><input type="number" name="mod"
                            placeholder="Modifier"></td>
                        <td><input type="number" name="roll"
                            placeholder="Roll"></td>
                        <td class="total">--</td>
                        <td><input type="checkbox" name="hidden"></td>
                        <td><input id="add" type="submit" value="Add +"></td>
                    </form>
                </tr>
                <?php } ?>
            </tbody>
        </table>

        <?php if ($dup) { ?>

        <h2>Conflicts</h2>

        <table>
            <thead>
                <tr>
                    <th class="id">ID</th>
                    <th>Name</th>
                    <th>Character Name</th>
                    <th width="20px">Init</th>
                    <th width="90px">Second roll</th>
                </tr>
            </head>
            <tbody>
            <?php for ($i = 0; $i < count($dup); $i++) {
                echo '<tr';
                if ($data[$i] -> sec_rank == 0) {
                    echo ' class="red"';
                }
                echo '>';?>
                    <td class="id"><?php echo $dup[$i] -> id; ?></td>
                    <td><?php echo $dup[$i] -> name; ?></td>
                    <td><?php echo $dup[$i] -> char_name; ?></td>
                    <td><?php echo $dup[$i] -> getTotal(); ?></td>
                    <?php
                        if (
                            $pUser -> perm > 1
                            //||
                            //$pUser -> id == $dup[$i] -> id
                        ) {
                    ?>
                    <td>
                        <input
                            form="main"
                            type="number"
                            name="secondary_<?php echo $dup[$i] -> id; ?>"
                            value="<?php echo $dup[$i] -> sec_rank; ?>"
                        >
                    </td>
                    <?php } else { ?>
                        <td><?php echo $dup[$i] -> sec_rank ?></td>
                    <?php } ?>
                </tr>
            <?php } ?>
            </tbody>
        </table>

        <?php } ?>

    </body>

</html>
