<?php

include('global.php');

$hidden = 0;
if ($pUser -> perm > 1 && $_POST['hidden'] == 'on') {
    $hidden = 1;
}

if (
    $pUser -> perm > 1 &&
    $_POST['char_name'] != null &&
    $_POST['mod'] != null &&
    $_POST['roll'] != null
) {

    $player = new Player(
        null,
        $_POST['char_name'],
        $_POST['char_name'],
        $_POST['mod'],
        $_POST['roll'],
        $_POST['char_name'],
        null,
        0,
        $hidden,
        0
    );

    if ($player -> write()) {
        header('Location: ' . $GLOBALS['home']);
    }

} else {
    echo 'Nope.<br />';
}

?>
