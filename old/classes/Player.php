<?php

class Player {

    public $id;             // unique ID
    public $name;           // player name
    public $char_name;      // character name
    public $mod;            // initiative modifier
    public $roll;           // die roll
    public $user;           // username
    public $pass;           // password
    public $perm;           // permission level
    public $hidden;         // 1 = hidden, 0 = shown
    public $sec_rank;       // secondary ranking value

    public function __construct(
        $id, $name, $char_name, $mod, $roll, $user, $pass, $perm, $hid, $sec) {
        $this -> id = $id;
        $this -> name = $name;
        $this -> char_name = $char_name;
        $this -> mod = $mod;
        $this -> roll = $roll;
        $this -> user = $user;
        $this -> pass = $pass;
        $this -> perm = $perm;
        $this -> hidden = $hid;
        $this -> sec_rank = $sec;
    }

    public function getTotal() { return ($this -> mod + $this -> roll); }

    public static function select_query($sql) {

        $result = mysqli_query($GLOBALS['conn'], $sql);

        if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
                $player = new Player(
                    $row['id'],
                    $row['player_name'],
                    $row['char_name'],
                    $row['modifier'],
                    $row['roll'],
                    $row['username'],
                    $row['password'],
                    $row['permission_level'],
                    $row['hidden'],
                    $row['secondary_rank']
                );
            }
        } else {
            return false;
        }

        return $player;

    }

    public static function bool_query($sql) {
        if ($GLOBALS['conn'] -> query($sql) == true) {
            return true;
        } else {
            echo "Error processing player: " . $GLOBALS['conn'] -> error;
            return false;
        }
    }

    public static function select_query_mult($sql) {

        $result = mysqli_query($GLOBALS['conn'], $sql);
        $players = array();

		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
                array_push($players,
                    new Player(
                        $row['id'],
                        $row['player_name'],
                        $row['char_name'],
                        $row['modifier'],
                        $row['roll'],
                        $row['username'],
                        $row['password'],
                        $row['permission_level'],
                        $row['hidden'],
                        $row['secondary_rank']
                    )
                );
			}
		} else {
            return false;
        }

		return $players;

    }

    public static function getAll() {
        return Player::select_query_mult(
            'SELECT *, (modifier + roll) as total
                FROM players
                WHERE permission_level <= 1 AND id > 2 AND hidden = 0
                ORDER BY total DESC, secondary_rank ASC'
        );
	}

    public static function getAdminAll() {
        return Player::select_query_mult(
            'SELECT *, (modifier + roll) as total
                FROM players
                WHERE permission_level <= 1 AND id > 2
                ORDER BY total DESC, secondary_rank ASC'
        );
    }

    public static function getById($id) {
		return Player::select_query(
            'SELECT * FROM players WHERE id="' . $id . '"'
        );
	}

    public static function getByPlayerName($name) {
		return Player::select_query(
            'SELECT * FROM players WHERE player_name="' . $name . '"'
        );
	}

    public static function getByUsername($username) {
		return Player::select_query(
            'SELECT * FROM players WHERE username="' . $username . '"'
        );
	}

    public static function getConflicts() {
        return Player::select_query_mult(
            'SELECT *, modifier + roll AS total
             FROM (
            	 SELECT modifier + roll AS dup_total
            	 FROM players
            	 WHERE id > 1 AND roll != 0 AND secondary_rank = 0
            	 GROUP BY dup_total
            	 HAVING COUNT(dup_total) >= 2
             ) AS dup, players
             WHERE players.modifier + players.roll = dup_total
             ORDER BY total DESC, id ASC;'
        );
    }

    public function write() {

		$sql = 'SELECT * FROM players';
		$result = mysqli_query($GLOBALS['conn'], $sql);
		$alreadyExists = false;

        if ($this -> roll == null) {
            $this -> roll = 0;
        }

		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				if (strcasecmp(
						str_replace(' ', '', $row['char_name']),
						str_replace(' ', '', $this -> char_name))
					== 0
				) {
					$alreadyExists = true;
					$match = $row['char_name'];
				}
			}
		}

		if (
            !$alreadyExists
            &&
            str_replace(' ', '', $this -> char_name) != ""
        ) {
			return Player::bool_query("INSERT INTO players (
                player_name,
				char_name,
                modifier,
                roll,
                username,
                password,
                permission_level,
                hidden,
                secondary_rank
			) VALUES (
				'" . $this -> name . "',
                '" . $this -> char_name . "',
                '" . $this -> mod . "',
                '" . $this -> roll . "',
                '" . $this -> user . "',
                '" . $this -> pass . "',
                '" . $this -> perm . "',
                '" . $this -> hidden . "',
                '" . $this -> sec_rank . "'
			)");
		} else {
			echo
				'The character "' .
				$this -> char_name .
				'" matches "' .
				$match .
				'", an already existing item.';
			;
			return false;
		}

	}

    public function update($id) {
        return Player::bool_query(sprintf(
            'UPDATE players
             SET player_name="%s",
                 char_name="%s",
                 modifier="%d",
                 roll="%d",
                 secondary_rank="%d",
                 hidden="%d"
             WHERE id="%d"',
            $this -> name,
            $this -> char_name,
            $this -> mod,
            $this -> roll,
            $this -> sec_rank,
            $this -> hidden,
            $id
        ));
    }

    public static function remove($id) {
        return Player::bool_query(
            'DELETE FROM players WHERE id="' . $id . '"'
        );
    }

    public function clearRoll() {
        return Player::bool_query(
            'UPDATE players
             SET
                roll="' . 0 . '",
                secondary_rank="' . 0 . '"'
        );
    }

};

?>
