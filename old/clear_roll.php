<?php

include('global.php');

$players = Player::getAll();

if ($pUser -> perm > 1) {
    for ($i = 0; $i < count($players); $i++) {
        $players[$i] -> clearRoll();
    }
    header('Location: ' . $GLOBALS['home']);
} else {
    echo 'Nope.';
}

?>
