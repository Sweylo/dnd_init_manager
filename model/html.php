<?php

class html {
	
	public $tag;
	public $contents;
	public $attr;
	private $self_closing;
	
	function __construct($tag, $attr, $contents) {
		
		$this -> tag = $tag;
		$this -> contents = $contents;
		
		if (is_array($attr)) {
			$this -> attr = $attr;
		} else {
			throw new Exception('The $attr parameter needs to be an array.');
		}
		
		$this -> self_closing = $tag == 'br' || $tag == 'hr' || $tag == 'input' || $tag == 'img';
		
	}
	
	function to_string() {
		
		$out = "<$this->tag";
		
		foreach ($this -> attr as $key => $attr) {
			$out .= " $key='$attr' ";
		}
		
		$out .= ($this -> self_closing) ? ' />' : ">$this->contents</$this->tag>";
		
		return $out;
		
	}
	
	function out() {
		echo $this -> to_string();
	}
	
}

?>